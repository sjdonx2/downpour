# TODO
+ Expose operations that only work on the Torrent type
+ Add different ways for deleting peer for the map (i.e. Remove peer from each key, entire key, just
  one peer from a key, Closing entire map)
+ Ensure that deletion correctly closes the handle(s) as well
+ Handle sending requests for the final piece which is smaller than the others
+ Torrent should keep track of how many pieces are left, there respective indicies, possibly the
  piece length
+ For writePiece: Don't write block to file, keep it until all blocks of the piece are downloaded,
  hash that with the piece hash string provided by the .torrent file
+ Add parser for peers given in dictionary model
+ Test the packages http-client, network-uri, http-types for correct url-encoding
+ Refactor bencode
+ Explore alternative to Arbitrary Message orphan instance
+ Add smart constructors for BEncode instead of access to the data constructors
+ Possible use packages that provide lifted functions (e.g. stm-lifted, etc.)
+ Explore logging frameworks
+ Possibly switch to a Hash/Map for the peers instead of a IntMap

## Remember
+ Keep one thread for every peer, and handle sending and recieving messages separately
+ get duped handle using hDuplicate from GHC.IO.Handle for seeking and writing to the file
