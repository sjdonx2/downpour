{-# LANGUAGE OverloadedStrings #-}
module Tests.Request (urlTest) where

import Control.Applicative
import Data.BEncode
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BC
import Network.Torrent.Request
import Network.HTTP.Client as HC
import Network.HTTP.Types
import Network.URI
import Network.Torrent.PeerId
import Test.Tasty
import Test.Tasty.HUnit

urlTest :: TestTree
urlTest = testGroup "Different constructors to test" [testHttpTypes]

genEncoder :: (B.ByteString -> B.ByteString) -> B.ByteString -> Dict -> IO String
genEncoder hf pid d = do
    h <- infoHash d
    let q = renderSimpleQuery True [ ("info_hash", h)
                                   , ("peer_id", hf pid)
                                   , ("port", "6881")
                                   , ("downloaded", "2345")
                                   , ("event", "started")
                                   , ("left", torrentLength d)
                                   ]
    return $ announce d ++ BC.unpack q

httpclientEncoder :: B.ByteString -> Dict -> IO String
httpclientEncoder pid d = do
    h <- infoHash' d
    url <- parseUrl $ announce d
    let uri = getUri $ setQueryString [ ("info_hash",  Just h)
                                      , ("peer_id",    Just pid)
                                      , ("port",       Just "6881")
                                      , ("downloaded", Just "2345")
                                      , ("event",      Just "started")
                                      , ("left",       Just $ torrentLength d)
                                      ] url
    return $ show uri

testHttpTypes :: TestTree
testHttpTypes = testGroup "Different encoding package"
    [ testCase "network-uri" $
        testPackage (genEncoder (BC.pack . normalizeEscape . BC.unpack))
    , testCase "http-client" $
        testPackage httpclientEncoder
    ]

getFromFile :: IO Dict
getFromFile = fromByteString <$> B.readFile "tests/data/horrible.torrent"

testPackage :: (B.ByteString -> Dict -> IO String) -> Assertion
testPackage ef = do
    d <- getFromFile
    pid <- getPeerIdByteString
    eurl <- newRequestPid pid d
    turl <- ef pid d
    eurl @=? turl
