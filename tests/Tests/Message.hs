{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
module Tests.Message (messageTests) where

import Control.Applicative
import qualified Data.ByteString as B
import Data.ByteString.Arbitrary
import Data.Bitstream hiding (length, replicate, null)
import Data.List
import Network.Torrent.Message
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

messageTests :: TestTree
messageTests = localOption (QuickCheckTests 10000) $ testGroup "Test Message parsing" [testHave, testKeepAlive, testBitfield, messageQC]

testHave :: TestTree
testHave = testGroup "Have messrem"
    [ testCase "decodeMessages . encodeMessage" $ do
        let m = Have 42
        singleMessageTest m
    ]

testKeepAlive :: TestTree
testKeepAlive = testGroup "KeepAlive messages"
    [ testCase "decodeMessages . encodeMessage" $
        singleMessageTest KeepAlive
    ]

testBitfield :: TestTree
testBitfield = testGroup "Bitfield messages"
    [ testCase "All 1's" $ do
        let m = BitField . pack $ replicate 8 True
        singleMessageTest m
    , testCase "All 0's" $ do
        let m = BitField . pack $ replicate 8 False
        singleMessageTest m
    , testCase "1's and 0's" $ do
        let m = BitField . pack $ intersperse True (replicate 4 False) ++ [True]
        singleMessageTest m
    ]

singleMessageTest :: Message -> Assertion
singleMessageTest m = case pdots m of
    Right [m'] -> m @=? m'
    Right e    -> assertFailure $ "Good parse, not equivalent: got e" ++ show e
    Left  e    -> assertFailure $ "Bad parse: got " ++ show e


pdots :: Message -> Either String [Message]
pdots = decodeMessages . encodeMessage

instance Arbitrary Message where
    arbitrary = oneof [ genKeepAlive, genHave, genChoke
                      , genUnchoke, genNotInterested
                      , genInterested, genRequest
                      , genCancel, genPiece, genBitField
                      ]
    shrink    = shrinkNothing

messageQC :: TestTree
messageQC = testProperty "QuickCheck decodeMessages . encodeMessage" (\m -> pdots m == return [m])

genKeepAlive, genChoke, genUnchoke, genInterested, genNotInterested :: Gen Message
genKeepAlive     = return KeepAlive
genUnchoke       = return Unchoke
genChoke         = return Choke
genInterested    = return Interested
genNotInterested = return NotInterested

genHave :: Gen Message
genHave = Have <$> arbitrary

genRequest :: Gen Message
genRequest = Request <$> arbitrary <*> arbitrary <*> arbitrary

genCancel :: Gen Message
genCancel = Cancel <$> arbitrary <*> arbitrary <*> arbitrary

genPiece :: Gen Message
genPiece = do
    ABS bs <- suchThat arbitrary (\(ABS bs) -> (not . B.null) bs)
    Piece <$> arbitrary <*> arbitrary <*> pure bs

genBitField :: Gen Message
genBitField = BitField <$> fmap pack (suchThat arbitrary $ \bs -> length bs `rem` 8 == 0 && length bs /= 0)
