{-# LANGUAGE OverloadedStrings #-}
module Tests.BEncode (bencodingTests) where

import Data.BEncode
import Data.Map (fromList)
import qualified Data.ByteString as B
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Golden

bencodingTests :: TestTree
bencodingTests = testGroup "Testing of BEncode" [decode, encode, files]

decode :: TestTree
decode = testGroup "BEncode to Haskell values"
    [ testCase "fromByteString" $
        fromByteString "d3:cow3:moo4:spam4:eggse" @?=
        Dict (fromList [("cow", BString "moo"), ("spam", BString "eggs")])
    ]

encode :: TestTree
encode = testGroup "Haskell values to BEncode"
    [ testCase "serializeDict" $
        serializeDict (fromByteString "d3:cow3:moo4:spam4:eggse") @?=
        "d3:cow3:moo4:spam4:eggse"
    ]

newTorrent :: FilePath -> FilePath -> IO ()
newTorrent fp tname = do
    f <- fmap (serializeDict . fromByteString) (B.readFile fp)
    B.writeFile tname f

files :: TestTree
files = testGroup "BEncode -> Haskell -> BEncode"
                  [horrible, underwater, deadfish]

horrible :: TestTree
horrible = goldenVsFile
    "horrible"
    "tests/data/horrible.torrent"
    "/tmp/horrible-test.torrent"
    (newTorrent "tests/data/horrible.torrent" "/tmp/horrible-test.torrent")

underwater :: TestTree
underwater = goldenVsFile
    "underwater"
    "tests/data/underwater.torrent"
    "/tmp/underwater-test.torrent"
    (newTorrent "tests/data/underwater.torrent"
              "/tmp/underwater-test.torrent")

deadfish :: TestTree
deadfish = goldenVsFile
    "deadfish"
    "tests/data/deadfish.torrent"
    "/tmp/deadfish-test.torrent"
    (newTorrent "tests/data/deadfish.torrent"
              "/tmp/deadfish-test.torrent")
