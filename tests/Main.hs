module Main where

import Tests.BEncode
import Tests.Message
import Tests.Request
import Test.Tasty

tests :: TestTree
tests = testGroup "Bittorrent client tests" [bencodingTests, messageTests, urlTest]

main :: IO ()
main = defaultMain tests
