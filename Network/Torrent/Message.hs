module Network.Torrent.Message
   ( Message(..)
   , encodeMessage
   , decodeMessages
   ) where

import              Control.Applicative
import              Control.Monad           (unless, forM_)
import              Data.Serialize
import qualified    Data.ByteString         as B
import              Data.Bitstream          (Bitstream)
import qualified    Data.Bitstream          as BS
import              Data.Bitstream.Packet
import qualified    Data.Vector.Storable    as V
import              Data.Word               (Word8, Word32)
import              Network

data Message = KeepAlive
             | Choke
             | Unchoke
             | Interested
             | NotInterested
             | Have Word32
             | BitField (Bitstream Right)

             | Request Word32       -- ^index
                       Word32       -- ^begin
                       Word32       -- ^length

             | Piece   Word32       -- ^index
                       Word32       -- ^begin
                       B.ByteString -- ^block

             | Cancel  Word32       -- ^index
                       Word32       -- ^begin
                       Word32       -- ^length
             | Port    PortNumber
             deriving (Show, Eq)

toWord8 :: Message -> Word8
toWord8 m = case m of
    Choke           -> 0
    Unchoke         -> 1
    Interested      -> 2
    NotInterested   -> 3
    Have _          -> 4
    BitField _      -> 5
    Request{}       -> 6
    Piece{}         -> 7
    Cancel{}        -> 8
    Port _          -> 9
    KeepAlive       -> error "toWord8: KeepAlive"

putMessageId :: Message -> Put
putMessageId = putWord8 . toWord8

encodeMessage :: Message -> B.ByteString
encodeMessage = runPut . putMessage

-- TODO: Clean up the end of this
-- TODO: Add tests for this function
--
{-| Generates the ByteString representation of each message
    Each message typically has a 4-byte(32-bits, big-endian) length first, then the associated
    1-byte(8-bits) message Id, given by putMessageId
-}
putMessage :: Message -> Put
putMessage m = case m of
    KeepAlive -> putWord32be 0

    Have i -> do putWord32be 5
                 putMessageId m
                 putWord32be i

    BitField b -> do let pkts = BS.toPackets b
                         len = (fromIntegral . V.length) pkts
                     putWord32be (1 + len)
                     putMessageId m
                     forM_ (V.toList . V.map toOctet $ pkts) putWord8

    Request i b l -> do putBigMessage 13 m i b
                        putWord32be l

    Piece i b blk -> do putBigMessage (fromIntegral $ 9 + B.length blk) m i b
                        putByteString blk

    Cancel i b l -> do putBigMessage 13 m i b
                       putWord32be l

    Port p  -> do putWord32be 3
                  putMessageId m
                  putWord16be (fromIntegral p)

    _ -> putWord32be 1 >> putMessageId m

putBigMessage :: Word32 -> Message -> Word32 -> Word32 -> Put
putBigMessage s m i b = do putWord32be s
                           putMessageId m
                           putWord32be i
                           putWord32be b

decodeMessages :: B.ByteString -> Either String [Message]
decodeMessages = runGet (some getMessage)

getMessage :: Get Message
getMessage = keepAlive     <|> choke  <|> unchoke  <|> interested
         <|> notInterested <|> piece  <|> have     <|> request
         <|> bitField      <|> cancel <|> port

-- | Short-circuits if there is a mismatch of the expected length
--   For messages that that a have a guaranteed static length
--   (i.e. Everything except bitfields and piece)
checkLength :: Word32 -> Get ()
checkLength len = getWord32be >>= \p -> unless (p == len) empty

-- | Ensures the minimum length of the message and returns it as an Int for
--   use with Data.Serialize.Get.getBytes
--   This is used for messages that have dynamic length and the bytes used for length
--   are needed to determine how much to parse (i.e. bitfields, piece)
getLength :: Word32 -> Get Int
getLength smallest = do pl <- getWord32be
                        unless (pl > smallest) empty
                        return . fromIntegral $ pl - smallest

-- | If the message if for the message and that of what was parsed is incorrect,
--   fail the parse
checkMessageId :: Message -> Get ()
checkMessageId m = getWord8 >>= \i -> unless (i == toWord8 m) empty

-- | Generic function for messages with a length of 1 followed by the message Id
--   E.g. un/choke, not/interested
noPayLoad :: Message -> Get Message
noPayLoad m = do checkLength 1
                 checkMessageId m
                 return m

keepAlive :: Get Message
keepAlive = do n <- getWord32be
               if n == 0 then return KeepAlive
                         else empty

choke :: Get Message
choke = noPayLoad Choke

unchoke :: Get Message
unchoke = noPayLoad Unchoke

interested :: Get Message
interested = noPayLoad Interested

notInterested :: Get Message
notInterested = noPayLoad NotInterested

bitField :: Get Message
bitField = do n <- getLength 1
              checkMessageId BitField{}
              BitField . BS.fromPackets . V.map fromOctet <$> V.replicateM n getWord8

piece :: Get Message
piece = do n <- getLength 9
           checkMessageId Piece{}
           Piece <$> getWord32be <*> getWord32be <*> getBytes n

have :: Get Message
have = do checkLength 5
          checkMessageId Have{}
          Have <$> getWord32be

request :: Get Message
request = do checkLength 13
             checkMessageId Request{}
             Request <$> getWord32be <*> getWord32be <*> getWord32be

cancel :: Get Message
cancel = do checkLength 13
            checkMessageId Cancel{}
            Cancel <$> getWord32be <*> getWord32be <*> getWord32be

port :: Get Message
port = do checkLength 3
          checkMessageId Port{}
          Port . fromIntegral <$> getWord16be
