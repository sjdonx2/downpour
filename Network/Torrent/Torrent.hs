{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Network.Torrent.Torrent
    ( Torrent(..)
    , TorrentM
    , PeerState(..)
    , Key
    , newTorrent
    , restartTorrent
    , getTorrentHandle
    , updatePeerMap
    , getPeerAtIndex
    , deletePeerMap
    , deletePeer
    , deletePeerMapIndex
    ) where

import           Control.Applicative
import           Control.Concurrent.STM
import           Control.Monad.State
import qualified Data.ByteString            as B
import           Data.HashMap.Strict        (HashMap)
import qualified Data.HashMap.Strict        as HM
import           Data.Word                  (Word32)
import           GHC.IO.Handle              (hDuplicate)
import           Network.Torrent.Peer
import           System.IO


-- TODO: Explore the use of a TQueue instead of an ordinary list
type Key = Word32
newtype PeerMap = PeerMap (TVar (HashMap Key [PeerState]))

data Torrent = Torrent { tPeerMap    :: PeerMap
                       , tFile       :: Handle
                       , tHash       :: TVar [B.ByteString]
                       , tLeft       :: TVar Integer
                       , tDownloaded :: TVar Integer
                       }

newtype TorrentM a = TorrentM (StateT Torrent IO a)
    deriving (Functor, Applicative, Alternative, Monad, MonadIO, MonadState Torrent)

newPeerMap :: IO PeerMap
newPeerMap = PeerMap <$> newTVarIO (HM.empty :: HashMap Key [PeerState])

restartTorrent :: [B.ByteString] -> Handle -> Integer -> Integer -> IO Torrent
restartTorrent hashes hdl left downloaded = Torrent <$> newPeerMap
                                                    <*> pure hdl
                                                    <*> newTVarIO hashes
                                                    <*> newTVarIO left
                                                    <*> newTVarIO downloaded

newTorrent :: [B.ByteString] -> Handle -> Integer -> IO Torrent
newTorrent hashes hdl left = restartTorrent hashes hdl left 0

getTorrentHandle :: TorrentM Handle
getTorrentHandle = gets tFile >>= liftIO . hDuplicate

updatePeerMap :: Word32 -> PeerState -> TorrentM ()
updatePeerMap k p' = do
    PeerMap pvar <- gets tPeerMap
    p <- liftIO $ copyPeerStateHandle p'
    liftIO . atomically . modifyTVar' pvar $ \pmap ->
        case HM.lookup k pmap of
            Nothing -> HM.insert k [p] pmap
            Just ps -> HM.insert k (p:ps) pmap

getPeerAtIndex :: Key -> TorrentM PeerState
getPeerAtIndex k = do
    PeerMap pvar <- gets tPeerMap
    liftIO . atomically $ do
        pmap <- readTVar pvar
        case HM.lookup k pmap of
            Nothing     -> retry
            Just []     -> retry
            Just (p:ps) -> writeTVar pvar pmap' >> return p
                where pmap' = HM.insert k ps pmap

deletePeerMapIndex :: Key -> TorrentM ()
deletePeerMapIndex = undefined

deletePeer :: PeerState -> TorrentM ()
deletePeer = undefined

deletePeerMap :: TorrentM ()
deletePeerMap = undefined

-- -- TODO: Should probably close the handle before deleting peer
-- deletePeerMapSTM :: Key -> PeerMap -> STM ()
-- deletePeerMapSTM k (PeerMap blk) = modifyTVar' blk (I.delete k)
--
-- -- TODO: Should probably close the handle before deleting peer
-- deletePeerMap :: Key -> PeerMap -> IO ()
-- deletePeerMap k b = atomically (deletePeerMapSTM k b)
--
-- deletePeerSTM :: PeerState -> PeerMap -> STM ()
-- deletePeerSTM = undefined
--
-- deletePeer :: PeerState -> PeerMap -> IO ()
-- deletePeer ps bmp = atomically $ deletePeerSTM ps bmp
--
-- deletePeerMapIndex :: Torrent -> IO ()
-- deletePeerMapIndex = undefined
