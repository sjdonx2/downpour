module Network.Torrent.PeerId
    ( getPeerId
    , getPeerIdByteString
    ) where

import              Control.Applicative ((<$>))
import              Control.Monad       (replicateM)
import              Data.ByteString
import qualified    Data.ByteString.Char8 as BC
import              Data.String
import              Network.URI
import              System.Random.MWC

newSuffix :: Int -> IO String
newSuffix n = normalizeEscape . BC.unpack . pack <$> (createSystemRandom >>= replicateM n . uniform)

getPeerIdByteString :: IO ByteString
getPeerIdByteString = createSystemRandom >>= fmap (fromString . normalizeEscape . BC.unpack . pack) . replicateM 12 . uniform

getPeerId :: IO String
getPeerId = ("-HS0100-" ++) <$> newSuffix 12
