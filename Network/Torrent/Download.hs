module Network.Torrent.Download where

import              Control.Monad
import qualified    Data.Bitstream              as BS
import              Control.Monad.Trans
import qualified    Data.ByteString             as B
import              Data.Word                   (Word32)
import              Network.Torrent.Message
import              Network.Torrent.Torrent
import              System.IO

sendMessage :: MonadIO m => Handle -> Message -> m ()
sendMessage h = liftIO . B.hPut h . encodeMessage

sendInterested :: Key -> TorrentM ()
sendInterested k = do
    PeerState _ h <- getPeerAtIndex k
    sendMessage h Interested

sendRequest :: Key -> Word32 -> Word32 -> TorrentM ()
sendRequest k index begin = do
    PeerState _ h <- getPeerAtIndex k
    let size = 2^(14 :: Int) :: Word32 -- recommended size of request
    sendMessage h (Request index begin size)

receiveMessage :: PeerState -> TorrentM ()
receiveMessage p@(PeerState _ h) = do
    closed <- liftIO $ hIsClosed h
    when closed (return ())
    b <- liftIO $ hWaitForInput h 500
    if not b then receiveMessage p
             else do res <- liftIO $ B.hGet h 4096 -- TODO: See if this is the best size
                     case decodeMessages res of
                        Left _ -> error "receiveMessage: parsing message"
                        -- TODO: Possibly fork message evaluation into its own thread?
                        Right ms -> evalMessages p ms >> receiveMessage p

evalMessages :: PeerState -> [Message] -> TorrentM ()
evalMessages p ms | null ms = return ()
                  | otherwise = forM_ ms $ \m -> evalMessage p m

evalMessage :: PeerState -> Message -> TorrentM ()
evalMessage p m = case m of
    Have i -> updatePeerMap i p
    BitField bs -> forM_ (BS.elemIndices True bs) $ \i ->
                        updatePeerMap i p
    _ -> error "notImplemented: evalMessage"
