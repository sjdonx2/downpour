{-# LANGUAGE OverloadedStrings #-}
module Network.Torrent.Request where

import              Control.Lens
import              Control.Monad
import              Data.BEncode
import qualified    Data.ByteString.Char8   as BC
import qualified    Data.ByteString.Lazy    as BL
import              Network.HTTP.Types.URI
import              Network.URI
import              Network.Torrent.Peer
import              Network.Torrent.PeerId
import              Network.Wreq

{-| Currently, using the normal @getwith@ from @Network.Wreq@ doesn't
    seem to encode the hash data for the trackers tested with to understand
    properly. This function manually builds the request url in order to work correctly
    TODO: Investigate how to get getWith to work correctly
-}
newRequestPid :: BC.ByteString -> Dict -> IO String
newRequestPid pid d = do
    hash <- infoHash d
    -- let a <=> b     = a <> "=" <> b
    --     qinfoHash   = "info_hash"  <=> normalizeEscape (BC.unpack hash)
    --     qpeerId     = "peer_id"    <=> normalizeEscape pid
    --     qport       = "port"       <=> "6881"
    --     qdownloaded = "downloaded" <=> "2345"
    --     qleft       = "left"       <=> torrentLength d
    --     qevent      = "event"      <=> "started"
    let hf = BC.pack . normalizeEscape . BC.unpack
        q = renderSimpleQuery True [ ("info_hash", hash)
                                   , ("peer_id", hf pid)
                                   , ("port", "6881")
                                   , ("downloaded", "2345")
                                   , ("event", "started")
                                   , ("left", torrentLength d)
                                   ]
    return $ announce d ++ BC.unpack q

newRequest :: Dict -> IO String
newRequest d = do
    h <- infoHash d
    pid <- getPeerIdByteString
    let url = announce d ++ BC.unpack queryStr
        hf = BC.pack . normalizeEscape . BC.unpack
        queryStr = renderSimpleQuery True [ ("info_hash", h)
                                          , ("peer_id", hf pid)
                                          , ("port", "6881")
                                          , ("downloaded", "2345")
                                          , ("event", "started")
                                          , ("left", torrentLength d)
                                          ]
    return url

trackerRequest :: Dict -> IO (Response BL.ByteString)
trackerRequest = newRequest >=> get

-- TODO: Explore using a non-lens approach
retrievePeers :: Dict -> IO [Peer]
retrievePeers d = do
    r <- trackerRequest d
    return $  r ^. responseBody . to fromLazyByteString . to parsePeers
