{-# LANGUAGE OverloadedStrings #-}
module Network.Torrent.Handshake
    ( Handshake
    , newHandshake
    , sendHandshake
    ) where

import           Control.Applicative        ((<$>))
import           Control.Monad              (replicateM)
import qualified Data.ByteString            as B
import           Data.ByteString.Builder
import           Data.Monoid                ((<>))
import           Data.Serialize.Get
import           Data.Word
import           Network
import           Network.Torrent.Peer
import           System.IO

data Handshake = HS Word8           -- ^pstrlen
                    B.ByteString    -- ^pstr
                    [Word8]         -- ^reserved
                    B.ByteString    -- ^infoHash
                    B.ByteString    -- ^pid
                    deriving (Eq)

newHandshake :: B.ByteString -> B.ByteString -> Handshake
newHandshake = HS strlen str res
    where strlen = fromIntegral (19 :: Int)
          str    = "BitTorrent protocol"
          res    = replicate 4 $ fromIntegral (0 :: Int)

buildHandshake :: Handshake -> Builder
buildHandshake (HS psl ps res h pid) = word8 psl
                                        <> byteString ps
                                        <> (byteString . B.pack) res
                                        <> byteString h
                                        <> byteString pid

getHandshake :: Get Handshake
getHandshake = do len   <- getWord8
                  pstr  <- getByteString (fromIntegral len)
                  resd  <- replicateM 4 getWord8
                  h     <- getByteString 20
                  pid   <- getByteString 20
                  return $ HS len pstr resd h pid

-- TODO: Possibly pass Peerstate instead of just peer
sendHandshake :: Handshake -> Peer -> IO ()
sendHandshake hs (Peer h p) = withSocketsDo $ do
    hdl <- connectTo h p
    hSetBuffering hdl NoBuffering
    hSetBinaryMode hdl True
    hPutBuilder hdl (buildHandshake hs)
    hs' <- runGet getHandshake <$> B.hGet hdl 256 -- minimum to account for
    case hs' of                                   -- size of handshake
        Left _ -> hClose hdl -- TODO: Handle errors better
        Right a | a /= hs   -> hClose hdl -- TODO: Handle errors better
                | otherwise -> undefined
