{-# LANGUAGE OverloadedStrings #-}
module Network.Torrent.Peer
    ( Peer(..)
    , PeerState(..)
    , connectToPeers
    , parsePeers
    , copyPeerStateHandle
    , closePeerState
    , closePeerStates
    ) where

import              Control.Applicative         ((<$>), (<*>), many)
import              Control.Monad               (replicateM)
import              Data.Serialize.Get
import              Data.BEncode                (Dict, peers)
import              Data.List                   (intercalate)
import              Data.Foldable
import              Data.Traversable
import              GHC.IO.Handle               (hDuplicate)
import              Network                     (HostName, PortID(..), connectTo)
import              System.IO

-- TODO: Look into if keeping this information around is necessary
data Peer = Peer HostName PortID -- TODO: PortNumber perhaps?
             deriving (Show)

data PeerState = PeerState Peer Handle

getPortID :: Get PortID
getPortID = PortNumber . fromIntegral <$> getWord16be

getHostName :: Get HostName
getHostName = do
    ps <- map show <$> replicateM 4 getWord8
    return $ intercalate "." ps

getPeer :: Get Peer
getPeer = Peer <$> getHostName <*> getPortID

getPeers :: Get [Peer]
getPeers = many getPeer

parsePeers :: Dict -> [Peer]
parsePeers b = case (runGet getPeers . peers) b of
    Left e -> error $ show e ++ "- getPeers: Failed to parse peers"
    Right ps -> ps

connectToPeer :: Peer -> IO PeerState
connectToPeer p@(Peer host pn) = PeerState p <$> connectTo host pn

connectToPeers :: [Peer] -> IO [PeerState]
connectToPeers ps = forM ps connectToPeer

copyPeerStateHandle :: PeerState -> IO PeerState
copyPeerStateHandle (PeerState p h) = PeerState p <$> hDuplicate h

closePeerState :: PeerState -> IO ()
closePeerState (PeerState _ h) = hClose h

closePeerStates :: Foldable f => f PeerState -> IO ()
closePeerStates ps = forM_ ps closePeerState
