module Data.BEncode.Types where

import Data.ByteString (ByteString)
import Data.Map.Strict (Map)

-- When building the Torrent record, top level should be a Map
-- So this allows for not having to pattern match on the other BEncode values
newtype Dict = Dict { dict :: Map ByteString BEncode }
                deriving (Show, Eq, Ord)

-- TODO: Is keeping the length in the data structure necessary
data BEncode = BString !ByteString
             | BInt    !Integer
             | BList   ![BEncode]
             | BDict   !Dict
             deriving (Show, Eq, Ord)

newBString :: ByteString -> BEncode
newBString = BString
