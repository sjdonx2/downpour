{-# LANGUAGE OverloadedStrings #-}
module Data.BEncode.Dict where

import              Control.Lens
import              Data.BEncode.Types
import              Data.BEncode.Encode
import qualified    Data.ByteString             as B
import qualified    Data.ByteString.Char8       as BC
import              Data.Maybe                  (fromJust)
import              Network.URI
import              OpenSSL.Digest              hiding (digest)
import              OpenSSL.Digest.ByteString

beToString :: BEncode -> String
beToString (BString s) = BC.unpack s
beToString (BInt i)    = show i
beToString a           = show a

beToBS :: BEncode -> B.ByteString
beToBS (BString s) = s
beToBS (BInt i)    = BC.pack . show $ i
beToBS _           = error "beToBS"

announce :: Dict -> String
announce = beToString . lookupDict "announce"

peers :: Dict -> B.ByteString
peers = beToBS . lookupDict "peers"

torrentLength :: Dict -> B.ByteString
torrentLength = beToBS . lookupDict "length" . getDict . info

getDict :: BEncode -> Dict
getDict be | (BDict d) <- be = d
           | otherwise       = undefined

infoHash :: Dict -> IO B.ByteString
infoHash d = do
    h <- infoHash' d
    return $ (BC.pack . normalizeEscape . BC.unpack) h

infoHash' :: Dict -> IO B.ByteString
infoHash' = fmap B.pack . digest SHA1 . serializeBEncode . info

info :: Dict -> BEncode
info = lookupDict "info"

lookupDictMaybe :: B.ByteString -> Dict -> Maybe BEncode
lookupDictMaybe s (Dict ds) = ds ^. at s

lookupDict :: B.ByteString -> Dict -> BEncode
lookupDict s d = fromJust $ lookupDictMaybe s d
