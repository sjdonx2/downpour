{-# LANGUAGE OverloadedStrings #-}
module Data.BEncode.Parser where

import              Prelude                              hiding (take)
import              Control.Applicative
import              Control.Monad                        (void)
import              Data.BEncode.Types
import              Data.ByteString                      hiding (take)
import              Data.Attoparsec.ByteString.Char8
import              Data.Map.Strict                      as M

parseBEncode :: ByteString -> Either String Dict
parseBEncode = parseOnly topLevelDict

beString :: Parser BEncode
beString = genByteStringParse BString

beString' :: Parser ByteString
beString' = genByteStringParse id

genByteStringParse :: (ByteString -> r) -> Parser r
genByteStringParse f = do
    n <- decimal
    void $ char ':'
    bs <- take n
    return $ f bs

beInteger :: Parser BEncode
beInteger = BInt <$ char 'i' <*> signed decimal <* char 'e'

-- | Generic function for making BDict's and BDict's inside of BEncode
gdict :: (Map ByteString BEncode -> r) -> Parser r
gdict con = do
    void $ char 'd'
    as <- many1 $ (,) <$> beString' <*> bencodeParsers
    void $ char 'e'
    return . con $ fromAscList as

topLevelDict :: Parser Dict
topLevelDict = gdict Dict

beDict :: Parser BEncode
beDict = gdict (BDict . Dict)

beList :: Parser BEncode
beList = BList <$ char 'l' <*> many1 bencodeParsers <* char 'e'

bencodeParsers :: Parser BEncode
bencodeParsers = beString <|> beInteger <|> beList <|> beDict
