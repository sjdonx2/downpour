{-# LANGUAGE OverloadedStrings #-}
module Data.BEncode.Encode where

import              Data.BEncode.Types
import qualified    Data.ByteString             as B
import qualified    Data.ByteString.Lazy        as BL
import              Data.ByteString.Builder
import              Data.Monoid
import              Data.Foldable
import              Data.Map

serializeBEncode :: BEncode -> B.ByteString
serializeBEncode = BL.toStrict . toLazyByteString . buildBEncode

serializeDict :: Dict -> B.ByteString
serializeDict = BL.toStrict . toLazyByteString . buildDict

buildBEncode :: BEncode -> Builder
buildBEncode (BString s)       = intDec (B.length s) <> char7 ':' <> byteString s
buildBEncode (BInt i)          = char7 'i' <> integerDec i <> char7 'e'
buildBEncode (BList as)        = char7 'l' <> foldMap buildBEncode  as <> char7 'e'
buildBEncode (BDict (Dict as)) = char7 'd' <> foldMap buildByteStringBEncode (toAscList as) <> char7 'e'

buildDict :: Dict -> Builder
buildDict (Dict as) = char7 'd' <> foldMap buildByteStringBEncode (toAscList as) <> char7 'e'

buildByteStringBEncode :: (B.ByteString, BEncode) -> Builder
buildByteStringBEncode (bs,be) = buildBEncode (newBString bs) <> buildBEncode be
