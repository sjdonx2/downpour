{-# LANGUAGE OverloadedStrings #-}
module Data.BEncode
    ( fromByteString
    , fromLazyByteString
    , announce
    , infoHash
    , infoHash'
    , peers
    , torrentLength
    , serializeBEncode
    , serializeDict
    , Dict(..)
    , BEncode(..)
    ) where

import              Data.BEncode.Types
import              Data.BEncode.Parser
import              Data.BEncode.Encode
import              Data.BEncode.Dict
import qualified    Data.ByteString             as B
import qualified    Data.ByteString.Lazy        as BL
import              Data.Attoparsec.ByteString  (parse, maybeResult, feed)
import              Data.Maybe                  (fromJust)

fromByteString :: B.ByteString -> Dict
fromByteString = fromJust . maybeResult . (`feed` "") . parse topLevelDict

fromLazyByteString :: BL.ByteString -> Dict
fromLazyByteString = fromByteString . BL.toStrict
