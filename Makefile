test:
	cabal configure --enable-tests
	cabal build -j4
	cabal test -j4

dep:
	cabal install --only-dependencies -j --enable-tests
